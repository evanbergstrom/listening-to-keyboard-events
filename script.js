let boxTop = 200; 
let boxLeft = 200;


document.addEventListener('keydown', logKey);

function logKey(e) {
  if (e.keyCode === 38) {
    document.getElementById("box").style.top = (boxTop + 10) + "px";
    return boxTop;
  } else if (e.keyCode === 40) {
    document.getElementById("box").style.top = (boxTop - 10) + "px";
    return boxTop;
  } else if (e.keyCode === 39) {
    document.getElementById("box").style.left = (boxLeft + 10) + "px";
    return boxLeft;
  } else if (e.keyCode === 37) 
    document.getElementById("box").style.left = (boxLeft - 10) + "px";
    return boxLeft;
  }